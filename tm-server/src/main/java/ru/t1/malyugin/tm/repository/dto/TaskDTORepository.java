package ru.t1.malyugin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractWBSDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Modifying
    @Query("UPDATE TaskDTO t SET t.projectId = :projectId WHERE t.id = :taskId")
    void setProjectById(@NotNull @Param("taskId") String taskId,
                        @Nullable @Param("projectId") String projectId);

}