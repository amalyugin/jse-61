package ru.t1.malyugin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@NotNull M model);

    long getSize();

    void clear();

    @Nullable
    M findOneById(@Nullable String id);

    void removeById(@Nullable String id);

    @NotNull
    List<M> findAll();

}