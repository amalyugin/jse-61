package ru.t1.malyugin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;

public interface IProjectDTOService extends IWBSDTOService<ProjectDTO> {

    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}