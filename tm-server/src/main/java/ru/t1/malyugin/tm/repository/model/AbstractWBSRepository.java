package ru.t1.malyugin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.malyugin.tm.model.AbstractWBSModel;
import ru.t1.malyugin.tm.model.User;

import java.util.List;

@NoRepositoryBean
public interface AbstractWBSRepository<M extends AbstractWBSModel> extends AbstractUserOwnedRepository<M> {

    @NotNull
    List<M> findAllByUser(@NotNull User user, @NotNull Sort sort);

}